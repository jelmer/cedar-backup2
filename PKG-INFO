Metadata-Version: 1.0
Name: CedarBackup2
Version: 2.27.0
Summary: Implements local and remote backups to CD/DVD media.
Home-page: https://bitbucket.org/cedarsolutions/cedar-backup2
Author: Kenneth J. Pronovici
Author-email: pronovic@ieee.org
License: Copyright (c) 2004-2011,2013-2017 Kenneth J. Pronovici.  Licensed under the GNU GPL.
Description: 
        Cedar Backup is a software package designed to manage system backups for a
        pool of local and remote machines.  Cedar Backup understands how to back up
        filesystem data as well as MySQL and PostgreSQL databases and Subversion
        repositories.  It can also be easily extended to support other kinds of
        data sources.
        
        Cedar Backup is focused around weekly backups to a single CD or DVD disc, with
        the expectation that the disc will be changed or overwritten at the beginning
        of each week.  If your hardware is new enough, Cedar Backup can write
        multisession discs, allowing you to add incremental data to a disc on a daily
        basis.
        
        Alternately, Cedar Backup can write your backups to the Amazon S3 cloud
        rather than relying on physical media.
        
        Besides offering command-line utilities to manage the backup process, Cedar
        Backup provides a well-organized library of backup-related functionality,
        written in the Python programming language.
        
Keywords: local,remote,backup,scp,CD-R,CD-RW,DVD+R,DVD+RW
Platform: Any
