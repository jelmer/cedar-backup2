#!/bin/sh

# Make sure $ADTTMP is available
if [ "${ADTTMP}"F = "F" ]; then
   echo "Error: expected environment variable ADTTMP is not set."
   exit 1
fi

# Make sure $ADT_ARTIFACTS is available
if [ "${ADT_ARTIFACTS}"F = "F" ]; then
   echo "Error: expected environment variable ADT_ARTIFACTS is not set."
   exit 1
fi

# Determine locations of required tools
SOURCE_TREE="${PWD}"
CBACK="/usr/bin/cback"

# Set up some file and directory locations
WORKING="${ADTTMP}/backup/working"
COLLECT="${ADTTMP}/backup/collect"
STAGING="${ADTTMP}/backup/staging"
DATA="${ADTTMP}/data"
LOG="/var/log/cback.log"
CONFIG="/etc/cback.conf"
TODAY=`date +"%Y/%m/%d"`
YEAR=`date +"%Y"`
SHAFILE=`echo "${DATA}" | sed 's/^\///' | sed 's/\//-/g' | sed 's/$/.sha/'`
TARFILE=`echo "${DATA}" | sed 's/^\///' | sed 's/\//-/g' | sed 's/$/.tar.gz/'`

# Print a test summary
echo ""
echo "========================================================================="
echo "Running ${0}"
echo "========================================================================="
echo "SOURCE_TREE..: ${SOURCE_TREE}"
echo "ADTTMP.......: ${ADTTMP}"
echo "ADT_ARTIFACTS: ${ADT_ARTIFACTS}"
echo "CBACK........: ${CBACK}"
echo "========================================================================="
echo ""

# Always run tests from within $ADTTMP
cd ${ADTTMP}

# Create the working directory
echo "Working directory is: ${WORKING}"
mkdir -p ${WORKING}
if [ ! -d ${WORKING} ]; then
   echo "Error: failed to create working directory"
   exit 1
fi

# Create the collect directory
echo "Collect directory is: ${COLLECT}"
mkdir -p ${COLLECT}
if [ ! -d ${COLLECT} ]; then
   echo "Error: failed to create collect directory"
   exit 1
fi

# Create the staging directory
echo "Staging directory is: ${STAGING}"
mkdir -p ${STAGING}
if [ ! -d ${STAGING} ]; then
   echo "Error: failed to create staging directory"
   exit 1
fi

# Create the data directory
echo "Data directory is: ${DATA}"
mkdir -p ${DATA}
if [ ! -d ${DATA} ]; then
   echo "Error: failed to create data directory"
   exit 1
fi

# Extract tree5.tar.gz to create data in the source directory
echo "Extracting test data: ${SOURCE_TREE}/testcase/data/tree15.tar.gz"
tar zxvf ${SOURCE_TREE}/testcase/data/tree15.tar.gz -C ${DATA}
if [ $? != 0 ]; then
   echo "Error: failed to extract tree15.tar.gz to source directory"
   exit 1
fi

# Build a basic configuration file 
echo "Building basic configuration in: ${CONFIG}"
rm -f ${CONFIG}

echo "<?xml version=\"1.0\"?>"                                                          >> ${CONFIG}
echo "<cb_config>"                                                                      >> ${CONFIG}
echo "   <reference>"                                                                   >> ${CONFIG}
echo "      <author>simplebackup</author>"                                              >> ${CONFIG}
echo "      <revision>1</revision>"                                                     >> ${CONFIG}
echo "      <description>Debian Continous Integration</description>"                    >> ${CONFIG}
echo "   </reference>"                                                                  >> ${CONFIG}
echo "   <options>"                                                                     >> ${CONFIG}
echo "      <starting_day>tuesday</starting_day>"                                       >> ${CONFIG}
echo "      <working_dir>${WORKING}</working_dir>"                                      >> ${CONFIG}
echo "      <backup_user>backup</backup_user>"                                          >> ${CONFIG}
echo "      <backup_group>backup</backup_group>"                                        >> ${CONFIG}
echo "      <rcp_command>/usr/bin/scp -B</rcp_command>"                                 >> ${CONFIG}
echo "   </options>"                                                                    >> ${CONFIG}
echo "   <peers>"                                                                       >> ${CONFIG}
echo "      <peer>"                                                                     >> ${CONFIG}
echo "         <name>localhost</name>"                                                  >> ${CONFIG}
echo "         <type>local</type>"                                                      >> ${CONFIG}
echo "         <collect_dir>${COLLECT}</collect_dir>"                                   >> ${CONFIG}
echo "      </peer>"                                                                    >> ${CONFIG}
echo "   </peers>"                                                                      >> ${CONFIG}
echo "   <collect>"                                                                     >> ${CONFIG}
echo "      <collect_dir>${COLLECT}</collect_dir>"                                      >> ${CONFIG}
echo "      <archive_mode>targz</archive_mode>"                                         >> ${CONFIG}
echo "      <ignore_file>.cbignore</ignore_file>"                                       >> ${CONFIG}
echo "      <dir>"                                                                      >> ${CONFIG}
echo "        <abs_path>${DATA}</abs_path>"                                             >> ${CONFIG}
echo "        <mode>incr</mode>"                                                        >> ${CONFIG}
echo "      </dir>"                                                                     >> ${CONFIG}
echo "   </collect>"                                                                    >> ${CONFIG}
echo "   <stage>"                                                                       >> ${CONFIG}
echo "      <staging_dir>${STAGING}</staging_dir>"                                      >> ${CONFIG}
echo "   </stage>"                                                                      >> ${CONFIG}
echo "   <purge>"                                                                       >> ${CONFIG}
echo "      <dir>"                                                                      >> ${CONFIG}
echo "         <abs_path>${COLLECT}</abs_path>"                                         >> ${CONFIG}
echo "         <retain_days>0</retain_days>"                                            >> ${CONFIG}
echo "      </dir>"                                                                     >> ${CONFIG}
echo "      <dir>"                                                                      >> ${CONFIG}
echo "         <abs_path>${STAGING}</abs_path>"                                         >> ${CONFIG}
echo "         <retain_days>0</retain_days>"                                            >> ${CONFIG}
echo "      </dir>"                                                                     >> ${CONFIG}
echo "   </purge>"                                                                      >> ${CONFIG}
echo "</cb_config>"                                                                     >> ${CONFIG}

echo "ls -l ${CONFIG}"
ls -l ${CONFIG}

if [ ! -f ${CONFIG} ]; then
   echo "Error: failed to create ${CONFIG}"
   exit 1
fi

cat ${CONFIG}

# Run the validate action
echo "Running: ${CBACK} --verbose --debug --stack validate"
${CBACK} --verbose --debug --stack validate
if [ $? != 0 ]; then
   echo "Error: validate command failed"
   exit 1
fi

echo "Checking log file ${LOG}"

echo "ls -l ${LOG}"
ls -l ${LOG}

if [ `ls -l ${LOG} | awk '{print $1}'` != "-rw-r-----" ]; then
   echo "Error: logfile does not have permissions -rw-r-----"
   exit 1
fi

if [ `ls -l ${LOG} | awk '{print $3":"$4}'` != "root:adm" ]; then
   echo "Error: logfile does not have ownership root:adm" 
   exit 1
fi

# Run the collect action
echo "Running: ${CBACK} --verbose --debug --stack --full collect"
${CBACK} --verbose --debug --stack --full collect
if [ $? != 0 ]; then
   echo "Error: collect command failed"
   exit 1
fi

echo "ls -l ${COLLECT}"
ls -l ${COLLECT}

echo "ls -l ${WORKING}"
ls -l ${WORKING}

echo "Checking: ${WORKING}/${SHAFILE}"

if [ ! -f ${WORKING}/${SHAFILE} ]; then
   echo "Error: did not find expected SHA file for source directory"
   exit 1
fi

echo "ls -l ${WORKING}/${SHAFILE}"
ls -l ${WORKING}/${SHAFILE}

if [ `ls -l ${WORKING}/${SHAFILE} | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: SHA file does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${COLLECT}/cback.collect"

if [ ! -f ${COLLECT}/cback.collect ]; then
   echo "Error: did not find expected collect indicator file"
   exit 1
fi

echo "ls -l ${COLLECT}/cback.collect"
ls -l ${COLLECT}/cback.collect

if [ `ls -l ${COLLECT}/cback.collect | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: cback.collect does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${COLLECT}/${TARFILE}"

if [ ! -f ${COLLECT}/${TARFILE} ]; then
   echo "Error: did not find expected tar file for source directory"
   exit 1
fi

echo "ls -l ${COLLECT}/${TARFILE}"
ls -l ${COLLECT}/${TARFILE}

if [ `ls -l ${COLLECT}/${TARFILE} | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: tar file not have ownership backup:backup" 
   exit 1
fi

mkdir tmp

tar zxvf ${COLLECT}/${TARFILE} -C tmp
if [ $? != 0 ]; then
   echo "Error: failed to extract collected tar file"
   exit 1
fi

diff -rq ${DATA}  tmp/${DATA}
if [ $? != 0 ]; then
   echo "Error: collected data does not match original source data"
   exit 1
fi

# Run the stage action
echo "Running: ${CBACK} --verbose --debug --stack --full stage"
${CBACK} --verbose --debug --stack --full stage
if [ $? != 0 ]; then
   echo "Error: stage command failed"
   exit 1
fi

echo "ls -l ${STAGING}"
ls -l ${STAGING}

echo "ls -l ${STAGING}/${TODAY}"
ls -l ${STAGING}/${TODAY}

echo "ls -l ${STAGING}/${TODAY}/localhost"
ls -l ${STAGING}/${TODAY}/localhost

echo "Checking: ${STAGING}/${TODAY}"

if [ ! -d ${STAGING}/${TODAY} ]; then
   echo "Error: stage action did not create expected directory for today"
   exit 1
fi

echo "ls -ld ${STAGING}/${TODAY}"
ls -ld ${STAGING}/${TODAY}

if [ `ls -ld ${STAGING}/${TODAY} | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: today's staging directory does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${STAGING}/${TODAY}/cback.stage"

if [ ! -f ${STAGING}/${TODAY}/cback.stage ]; then
   echo "Error: stage action did not create staging indicator file"
   exit 1
fi

echo "ls -l ${STAGING}/${TODAY}/cback.stage"
ls -l ${STAGING}/${TODAY}/cback.stage

if [ `ls -l ${STAGING}/${TODAY}/cback.stage | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: cback.stage does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${STAGING}/${TODAY}/localhost"

if [ ! -d ${STAGING}/${TODAY}/localhost ]; then
   echo "Error: stage action did not create expected directory for localhost"
   exit 1
fi

echo "ls -ld ${STAGING}/${TODAY}/localhost"
ls -ld ${STAGING}/${TODAY}/localhost

if [ `ls -ld ${STAGING}/${TODAY}/localhost | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: localhost directory does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${STAGING}/${TODAY}/localhost/cback.collect"

if [ ! -f ${STAGING}/${TODAY}/localhost/cback.collect ]; then
   echo "Error: stage action did not copy collect indicator file"
   exit 1
fi

echo "ls -l ${STAGING}/${TODAY}/localhost/cback.collect"
ls -l ${STAGING}/${TODAY}/localhost/cback.collect

if [ `ls -l ${STAGING}/${TODAY}/localhost/cback.collect | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: cback.collect does not have ownership backup:backup" 
   exit 1
fi

echo "Checking: ${STAGING}/${TODAY}/localhost/${TARFILE}"

if [ ! -f ${STAGING}/${TODAY}/localhost/${TARFILE} ]; then
   echo "Error: stage action did not copy collected tar file"
   exit 1
fi

echo "ls -l ${STAGING}/${TODAY}/localhost/${TARFILE}"
ls -l ${STAGING}/${TODAY}/localhost/${TARFILE}

if [ `ls -l ${STAGING}/${TODAY}/localhost/${TARFILE} | awk '{print $3":"$4}'` != "backup:backup" ]; then
   echo "Error: tar file does not have ownership backup:backup" 
   exit 1
fi

diff -q ${COLLECT}/${TARFILE} ${STAGING}/${TODAY}/localhost/${TARFILE}
if [ $? != 0 ]; then
   echo "Error: staged tar file does not match original collected tar file"
   exit 1
fi

# Run the purge action
echo "Running: ${CBACK} --verbose --debug --stack --full purge"
${CBACK} --verbose --debug --stack --full purge
if [ $? != 0 ]; then
   echo "Error: purge command failed"
   exit 1
fi

echo "ls -l ${COLLECT}"
ls -l ${COLLECT}

echo "ls -l ${STAGING}"
ls -l ${STAGING}

echo "Checking: ${COLLECT}/cback.collect"
if [ -f ${COLLECT}/cback.collect ]; then
   echo "Error: purge did not remove collect indicator file as expected"
   exit 1
fi

echo "Checking: ${COLLECT}/${TARFILE}"
if [ -f ${COLLECT}/${TARFILE} ]; then
   echo "Error: purge did not remove collected tar file as expected"
   exit 1
fi

echo "Checking: ${STAGING}/${TODAY}/localhost"
if [ -d ${STAGING}/${TODAY}/localhost ]; then
   echo "Error: stage action did not remove localhost directory as expected"
   exit 1
fi

# Close the test
echo ""

