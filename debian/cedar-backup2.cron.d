# /etc/cron.d/cedar-backup

#
# Below, comment out the appropriate set of scheduled tasks.  See the
# documentation in /usr/share/doc/cedar-backup2/manual for more information.
#

#Single machine (pool of one)
#30 00 * * * root  /usr/bin/cback all

# Client machine
#30 00 * * * root  /usr/bin/cback collect
#30 06 * * * root  /usr/bin/cback purge

# Master machine
#30 00 * * * root  /usr/bin/cback collect
#30 02 * * * root  /usr/bin/cback stage
#30 04 * * * root  /usr/bin/cback store
#30 06 * * * root  /usr/bin/cback purge

#
